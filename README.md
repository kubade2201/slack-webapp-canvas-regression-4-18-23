# slack-webapp-canvas-regression-4-18-23

### In Scope

## Test Cycle for Slack WebApp
The goal of this test cycle is to find any issues within the sections and features outlined in the provided test cases. Exploratory testing should also be conducted around the cases themselves, but all issues should relate to the test case sections/features and importantly should relate to the Spaces / Canvas functionality.

For slot claiming - check Test Case Instructions section below.

# Requirements
Devices:
You may only report bugs and complete exploratory testing against these environments:

Windows 10 +
Testing via Web Browsers
Chrome 90+
Firefox Latest
Mac 10.9 +
Testing via Web Browsers
Chrome 90+
Firefox Latest
Safari 14.1+
Linux - Any distro
Testing via Web Browsers
Chrome 90+
Firefox Latest
 
## TESTING FOCUS
Feature Summary: This is a full regression run on the Canvas feature.

Basic Directions on how to Create a Canvas: 
<https://slack.com/help/articles/4416442287251-Canvases--A-new-way-to-curate-collaborate-and-share--Pilot->

<strong>Testing Prerequisites:

<b>HOW TO TEST:

1. <b>REVIEW all instructions for any changes and MONITOR the cycle chat

2. <b>DESKTOP BROWSER TESTERS: Open the CORRECT BROWSER and CLEAR CACHE/DATA for a clean browsing session.

* OPEN browser DevTools / Console Logging and ensure you have preserve/persist logs checked when reproducing an issue.

* Please <b>USE ONLY the following workspaces to test: 

*<b> applause4mobile.enterprise.slack.com - Credentials:
<https://platform.utest.com/docs/1JQgwQYon7EK13Qp8xBsFLvOCxnvkTnh2OEthYDQC05NBLgHRVe2CATpQflPQaK7tmXwRqL6zwSUhiZ1hB99p3Qp8xBsEILc3RLzkkAK23Qp8xBsxgBYpKeGbQeHB7yvm34GfaryTDqxOClvHc>
applause4slack.slack.com - If you do not have credentials, please ask the TTLs in chat.

### You may need to create & populate additional channels & content to thoroughly conduct your testing. Please ask the TTL if you need something you can't create. 

* Please be sure to <b>create your own channels etc for any test cases asking you to modify channel status and/or sharing. Please do not make these changes on the default workspaces. If a test case requires admin changes, ensure that you revert those changes as soon as you're done testing them, so as not to disrupt other testers!

3. <b>CAUTION: NEVER use the '/feedback' slash command during ANY Slack testing!

4. <b>CHECK, CLAIM and promptly complete any available test cases by following the Test Case Instructions and the Testing Focus.

* If you did not get a slot, please use the TEST CASES and the testing focus as a guide for exploratory testing around the TEST CASE FUNCTIONALITY ONLY (everything else is out of scope.)

* To find valuable bugs, please create your own content/channels etc and explore deeply to really understand the functionality.

* Use the +1 feature for any bugs you encounter that have already been reported by another tester

5. <b>REPORT any issues you find by following the Issue Reporting Instructions

### Out of Scope

<b>Known Issues:

* Most of known issues are attached to the cycle - please refer to them before making a report.

* <b>Additional Canvas Known Issues can be found here:
<a href="https://platform.utest.com/docs/1JQgwQYon7EK13Qp8xBsFLvOCxnvkTnh2OEthYDQC05NBLgHQfcyjpm8uyMJLJHjHQXVZHD48U7to8fZGuMy9fiWgIb7nW%2Bb2swH91dEbp%2BedqnprwxqhyC06s3Qp8xBswHe1hLjUro0"><https://platform.utest.com/docs/1JQgwQYon7EK13Qp8xBsFLvOCxnvkTnh2OEthYDQC05NBLgHQfcyjpm8uyMJLJHjHQXVZHD48U7to8fZGuMy9fiWgIb7nW%2Bb2swH91dEbp%2BedqnprwxqhyC06s3Qp8xBswHe1hLjUro0><a/>

<b>Do Not Report:​

* Bugs that cannot be reproduced more than twice

* Usability/ Experience (UX) bugs. However genuine User Interface (UI) bugs are in scope.

* Social-sharing aspects of the app unless you are 100% certain that the test accounts you are using are completely PRIVATE AND LOCKED. This means that your first test account is only connected with your other test account, both of which are private and locked. When testing is complete, please remove all artifacts from testing – tweets, posts, messages, etc.

* Use of Emulators

* Any Beta OS

* Any website external to the application (example: Google Maps)

* Security and Invasive testing

* Boundary and Field Validation including special characters and space

* Email integration (notification emails are disabled) (unless it is a live application)

* External links in app content, they have been disabled.

* Devices that do not match what is listed in the REQUIREMENTS section in the IN SCOPE section

### Issue Reporting Instructions

<b>MANDATORY - You must follow the below criteria for all issue reports: 

<b>IMPORTANT - If unsure, please use the FULL Requirements Guide here:
<http://bit.ly/2wt3l0L>

1. Issue Title - A concise yet descriptive title formatted as below including capitals, spaces and punctuation as shown

* Format ALL bug titles as follows INCLUDING CAPITALS & SPACES: [Canvas] - Short Description

​2. Priority - See Full Guide Link above

3. My Environments

* You must ONLY select the EXACT MATCHING ENVIRONMENT that you have discovered the issue whilst testing on.

* Only select additional environments if you have ALSO TESTED and verified the same issue occurs on that exact matching environment also.

* If the exact matching environment you are testing on is not shown, then you MUST UPDATE YOUR DEVICES in your tester profile BEFORE SUBMITTING THE ISSUE REPORT!

4. Action Performed - See Full Guide Link above

5. Result - See Full Guide Link above

6. Error Message - See Full Guide Link above

7. Additional Environment Info - See Full Guide Link above

8. Attachments

1. Video Capture AND Screenshot

* Please ensure you rotate the video/image and attach in upright position please. 

* Mark on the screenshots/video where/what the problem is

* All Issues MUST INCLUDE BOTH - IMAGES & VIDEO of the issue, unless it is clearly shown with only an image.

* .mp4 format for ALL video is mandatory (Use http://handbrake.fr/ to compress ALL videos before uploading)

2. Capture & Attach Logs

* DESKTOPS/LAPTOPS (Testing Via Browser)

* Browser Console Log is REQUIRED for all issues except Visual
See Full Guide Link above for console log capture instructions if needed. (.txt or .log format for all logs) 

### Test Case Instructions

1. Initially, only Linux test cases will be available for claiming! If we need more coverage, then we will release slots for other OSs.

2. Ask for a test case in chat. Be sure to include your email so you get access to the sheet. Access to the sheet will only be given if you have been slotted by a TTL.

3. After you are slotted, please access the following sheet to view open slots: https://platform.utest.com/docs/1JQgwQYon7EK13Qp8xBsFLvOCxnvkTnh2OEthYDQC05NBLgHTEgXGwrDvD1jtQkSLQaVLCoHu8WSs3Qp8xBsWuM54yWxulcJxJaDoEUle7KseZ4XzL17amlQxA5FeQymXbd1jzq%2BVJY%2B

1. Access requests to above link will be ignored. You will only get access after being slotted by a TTL.

4. Look for your assignment on the Slotting sheet and fill the data accordingly.

5. ONLY AFTER you got a slot, then you can claim a platform test case. Make sure to input it on your slotting as well.
Please be sure to finish your test case before the cycle locks.

Thank you!

# Attachments
[NDA](https://utest-dl.s3.amazonaws.com/9027/27447/410826/ndas/uTest-TesterNDA-2022.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230419T101321Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230419%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=1c545a6d3e852e6b2a2a01d753f6615ea22b5fedcaffe114450a375bfcaa6e33)

[TEST URLS](https://utest-dl.s3.amazonaws.com/9027/27447/410826/1/Slack_Canvas_0329_Web.xlsx?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230419T101528Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230419%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=56746aaffa9b3ba7192d62fc82d0cbd3e4a2e6f8ffa060ff7671b43e5c76ed61)

### Team Contact Information

If you have any questions about this cycle, the cycle chat is the best way to discuss them.


### TEST TEAM LEADS:

The TTLs for this Slack product are listed below.  They are responsible for responding to questions posted in chat and assisting with bug triage. Please keep in mind their time zones, as answers may take longer outside of normal business hours.

* Ana Laura Machado (GMT -3)
* Joel Sekabembe (GMT +3)
* Miguel Gonzalez (GMT -5)
* Tomas Simonkay (GMT +2)

<b>TEST ENGINEERS (SLACK MOBILE):

If someone on the TTL team does not respond within a reasonable amount of time, please reach out to a Test Engineer:

* Guilherme Leite (ibarrett@applausemail.com) (GMT -3)
* Ioji Barrett (ibarrett@applausemail.com) (GMT -3)
* Thiago Scarani (tscarani@applausemail.com) (GMT -3)

<b>TESTING SERVICES MANAGER:

If there is an emergency, something needs immediate escalation or you haven't heard back from the Test Engineers in over 18 hours, please contact the TSM:

* Renan Portocarrero (rportocarrero@applausemail.com)
